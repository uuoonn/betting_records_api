package com.lyj.bettingrecordsapi.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter

public class BettingRecord {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MemberId", nullable = false)
    private Member member;

    @Column(nullable = false)
    private LocalDate datePrize;

    @Column(nullable = false)
    private Double price;

}
