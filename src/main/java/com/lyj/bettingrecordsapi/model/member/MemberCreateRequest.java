package com.lyj.bettingrecordsapi.model.member;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter

public class MemberCreateRequest {
    private String name;
    private LocalDate dateBirth;
    private String etcMemo;
}
