package com.lyj.bettingrecordsapi.model.member;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class MemberMemoChangeRequest {
    private String etcMemo;
}
