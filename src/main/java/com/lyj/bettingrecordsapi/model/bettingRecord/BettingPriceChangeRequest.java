package com.lyj.bettingrecordsapi.model.bettingRecord;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class BettingPriceChangeRequest {
    private Double price;
}
