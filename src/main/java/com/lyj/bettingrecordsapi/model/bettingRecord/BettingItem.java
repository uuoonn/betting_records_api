package com.lyj.bettingrecordsapi.model.bettingRecord;

import jakarta.persistence.Column;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter

public class BettingItem {
    private String memberName;
    private LocalDate datePrize;
}
