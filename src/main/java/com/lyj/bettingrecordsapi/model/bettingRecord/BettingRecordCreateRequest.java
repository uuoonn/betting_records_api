package com.lyj.bettingrecordsapi.model.bettingRecord;

import com.lyj.bettingrecordsapi.entity.Member;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter

public class BettingRecordCreateRequest {
    private LocalDate datePrize;
    private Double price;
}
