package com.lyj.bettingrecordsapi.repository;

import com.lyj.bettingrecordsapi.entity.BettingRecord;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BettingRecordRepository extends JpaRepository<BettingRecord,Long> {
}
