package com.lyj.bettingrecordsapi.repository;

import com.lyj.bettingrecordsapi.entity.Member;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MemberRepository extends JpaRepository<Member,Long> {
}
