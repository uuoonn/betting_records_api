package com.lyj.bettingrecordsapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BettingRecordsApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(BettingRecordsApiApplication.class, args);
	}

}
