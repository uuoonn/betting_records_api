package com.lyj.bettingrecordsapi.service;

import com.lyj.bettingrecordsapi.entity.BettingRecord;
import com.lyj.bettingrecordsapi.entity.Member;
import com.lyj.bettingrecordsapi.model.bettingRecord.BettingItem;
import com.lyj.bettingrecordsapi.model.bettingRecord.BettingPriceChangeRequest;
import com.lyj.bettingrecordsapi.model.bettingRecord.BettingRecordCreateRequest;
import com.lyj.bettingrecordsapi.model.bettingRecord.BettingResponse;
import com.lyj.bettingrecordsapi.repository.BettingRecordRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor

public class BettingRecordService {
    private final BettingRecordRepository bettingRecordRepository;

    public void setBetting(Member member,BettingRecordCreateRequest request){
        BettingRecord addData = new BettingRecord();
        addData.setMember(member);
        addData.setDatePrize(request.getDatePrize());
        addData.setPrice(request.getPrice());

        bettingRecordRepository.save(addData);

    }

    public List<BettingItem> getBettingItems(){
        List<BettingRecord> originList = bettingRecordRepository.findAll();

        List<BettingItem> result = new LinkedList<>();
        for(BettingRecord bettingRecord :originList){
            BettingItem addItem = new BettingItem();
            addItem.setMemberName(bettingRecord.getMember().getName());
            addItem.setDatePrize(bettingRecord.getDatePrize());

            result.add(addItem);
        }
        return result;
    }

    public BettingResponse getBetting(long id){
        BettingRecord bettingRecord = bettingRecordRepository.findById(id).orElseThrow();
        BettingResponse response = new BettingResponse();
        response.setMemberName(bettingRecord.getMember().getName());
        response.setDatePrize(bettingRecord.getDatePrize());
        response.setPrice(bettingRecord.getPrice());

        return response;
    }

    public void putRecord(long id, BettingPriceChangeRequest request) {
        BettingRecord originData = bettingRecordRepository.findById(id).orElseThrow();
        originData.setPrice(request.getPrice());

        bettingRecordRepository.save(originData);
    }

    public void delRecord(long id){
        bettingRecordRepository.deleteById(id);
    }



}
