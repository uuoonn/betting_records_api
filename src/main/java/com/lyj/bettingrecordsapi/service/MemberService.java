package com.lyj.bettingrecordsapi.service;

import com.lyj.bettingrecordsapi.entity.Member;
import com.lyj.bettingrecordsapi.model.member.MemberCreateRequest;
import com.lyj.bettingrecordsapi.model.member.MemberItem;
import com.lyj.bettingrecordsapi.model.member.MemberMemoChangeRequest;
import com.lyj.bettingrecordsapi.model.member.MemberResponse;
import com.lyj.bettingrecordsapi.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor

public class MemberService {
    private final MemberRepository memberRepository;

    public Member getData(Long id){
        return memberRepository.findById(id).orElseThrow();

    }


    public void setMember(MemberCreateRequest request){
        Member addData = new Member();
        addData.setName(request.getName());
        addData.setDateBirth(request.getDateBirth());
        addData.setEtcMemo(request.getEtcMemo());

        memberRepository.save(addData);
    }


    public List<MemberItem> getMembers(){
        List<Member> originList = memberRepository.findAll();

        List<MemberItem> result = new LinkedList<>();
        for (Member member:originList){
            MemberItem addItem = new MemberItem();
            addItem.setId(member.getId());
            addItem.setName(member.getName());
            addItem.setDateBirth(member.getDateBirth());

            result.add(addItem);
        }
        return result;
    }


    public MemberResponse getMember(long id){
        Member originData = memberRepository.findById(id).orElseThrow();
        MemberResponse response = new MemberResponse();
        response.setId(originData.getId());
        response.setName(originData.getName());
        response.setDateBirth(originData.getDateBirth());
        response.setEtcMemo(originData.getEtcMemo());

        return response;
    }

    public void putMemberMemo (long id, MemberMemoChangeRequest request){
        Member originData = memberRepository.findById(id).orElseThrow();
        originData.setEtcMemo(request.getEtcMemo());

        memberRepository.save(originData);
    }


}
