package com.lyj.bettingrecordsapi.configure;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import lombok.RequiredArgsConstructor;
import org.springdoc.core.models.GroupedOpenApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@OpenAPIDefinition(
        info = @Info(title = "커피룰렛 App",
                description = "커피내기에 진 사람은 누구고 얼마를 냈을까",
                version = "v1"))
@RequiredArgsConstructor
@Configuration

public class SwaggerConfig {

    @Bean
    public GroupedOpenApi v1OpenApi() {
        String[] paths = {"/v1/**"};

        return GroupedOpenApi.builder()
                .group("커피룰렛 API v1")
                .pathsToMatch(paths)
                .build();
    }

}
