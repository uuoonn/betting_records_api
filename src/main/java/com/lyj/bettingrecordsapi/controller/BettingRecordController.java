package com.lyj.bettingrecordsapi.controller;

import com.lyj.bettingrecordsapi.entity.Member;
import com.lyj.bettingrecordsapi.model.bettingRecord.BettingItem;
import com.lyj.bettingrecordsapi.model.bettingRecord.BettingPriceChangeRequest;
import com.lyj.bettingrecordsapi.model.bettingRecord.BettingRecordCreateRequest;
import com.lyj.bettingrecordsapi.model.bettingRecord.BettingResponse;
import com.lyj.bettingrecordsapi.service.BettingRecordService;
import com.lyj.bettingrecordsapi.service.MemberService;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/betting-record")

public class BettingRecordController {
    private final MemberService memberService;
    private final BettingRecordService bettingRecordService;


    @PostMapping("/record-create/{memberId}")
    public String setBetting(@PathVariable long memberId, @RequestBody BettingRecordCreateRequest request){
        Member member = memberService.getData(memberId);
        bettingRecordService.setBetting(member,request);

        return "정보 입력 완료";
    }

    @GetMapping("/more-record")
    public List<BettingItem> getBettingItems(){
        return bettingRecordService.getBettingItems();
    }

    @GetMapping("/detail/{id}")
    public BettingResponse getBetting(@PathVariable long id){
        return bettingRecordService.getBetting(id);
    }

    @PutMapping("/change-price/{id}")
    public String putRecord(@PathVariable long id, @RequestBody BettingPriceChangeRequest request){
        bettingRecordService.putRecord(id,request);

        return "가격 변경 완료";
    }

    @DeleteMapping("/del-record/{id}")
    public String delRecord(@PathVariable long id){
        bettingRecordService.delRecord(id);
        return "기록 삭제 완료";
    }
}
