package com.lyj.bettingrecordsapi.controller;

import com.lyj.bettingrecordsapi.model.member.MemberCreateRequest;
import com.lyj.bettingrecordsapi.model.member.MemberItem;
import com.lyj.bettingrecordsapi.model.member.MemberMemoChangeRequest;
import com.lyj.bettingrecordsapi.model.member.MemberResponse;
import com.lyj.bettingrecordsapi.service.MemberService;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/betting")
public class MemberController {
    private final MemberService memberService;

    @PostMapping("/member-create")
    public String setMember(@RequestBody MemberCreateRequest request){
        memberService.setMember(request);

        return "멤버 정보 입력 완료";
    }

    @GetMapping("/all-member")
    public List<MemberItem> getMembers(){
        return memberService.getMembers();
    }

    @GetMapping("/detail-member/{id}")
    public MemberResponse getMember(@PathVariable long id){
        return memberService.getMember(id);
    }

    @PutMapping("/memo-edit/{id}")
    public String putMemberMemo(@PathVariable long id ,@RequestBody MemberMemoChangeRequest request){
        memberService.putMemberMemo(id,request);
        return "메모 수정 완료";
    }

}
